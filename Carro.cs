using System;
namespace Ejercicio2{
    class Carro{
        public string color;
        public string marca;
        public int anyModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;
        public Motor miMotor;
        public Carro(){
            miMotor=new Motor();
            miTanque= new Tanque();
        }
        public void acelerar(){
            Console.WriteLine("Estoy acelerando");
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);
            Console.WriteLine("Remanente de combustible: "+ miTanque.obtenerLitros().ToString());
        }
        public void frenar(){
            Console.WriteLine("Estoy frenando");
        }
        public void obtenerCaracteristicas(){
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color: "+color);
            Console.WriteLine("Marca: "+marca);
            Console.WriteLine("Año del Modelo: "+anyModelo.ToString());
            Console.WriteLine("Número de Puertas: "+numeroPuertas.ToString());
            Console.WriteLine("Precio: "+precio.ToString());
            Console.WriteLine("Tipo de Carburador: " +miMotor.obtenerNumeroCilindros());
            Console.WriteLine("Número de Cilindros: "+miMotor.obtenerNumeroCilindros().ToString());
            Console.WriteLine("Tipo de Combustuble: "+miMotor.TipoCombustible);
        }
    }
}